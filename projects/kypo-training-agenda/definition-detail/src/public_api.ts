/*
 * Public API Surface of entry point kypo-training-agenda/definition-detail
 */
export * from './components/training-definition-detail-components.module';
export * from './components/adaptive-definition-detail-components.module';
