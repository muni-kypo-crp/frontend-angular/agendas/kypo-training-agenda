# TrainingAgenda

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.0.2.

## Code scaffolding

Run `ng generate component component-name --project training-agenda` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module --project training-agenda`.
> Note: Don't forget to add `--project training-agenda` or else it will be added to the default project in your `angular.json` file. 

## Build

Run `ng build training-agenda` to build the project. The build artifacts will be stored in the `dist/` directory.

## Publishing

After building your library with `ng build training-agenda`, go to the dist folder `cd dist/training-agenda` and run `npm publish`.

## Running unit tests

Run `ng test training-agenda` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Example

To see the library in work and to see example setup, you can run the example app.
To run the example you need to run [KYPO Training Service](https://gitlab.ics.muni.cz/muni-kypo-crp/backend-java/kypo-training) or have access to a running instance and provide the URL to the service in when importing API module.

Alternatively, you can run a json-server, which provides an example mocked DB with necessary endpoints. It is located in the [kypo-trainings](https://gitlab.ics.muni.cz/muni-kypo-crp/frontend-angular/kypo-trainings) project and can be run via `npm run api`.

1. Clone this repository
1. Run `npm install`
1. Run `ng serve --ssl`
1. See the app at `https://localhost:4200`
