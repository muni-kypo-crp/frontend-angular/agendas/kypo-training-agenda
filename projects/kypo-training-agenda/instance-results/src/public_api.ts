/*
 * Public API Surface of entry point kypo-training-agenda/instance-results
 */
export * from './components/training-instance-results-components.module';
export * from './components/training-instance-results.component';
