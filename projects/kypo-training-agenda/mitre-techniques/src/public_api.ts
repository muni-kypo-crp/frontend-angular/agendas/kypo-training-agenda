/*
 * Public API Surface of entry point kypo-training-agenda/mitre-techniques
 */

export * from './components/mitre-techniques.component';
export * from './components/mitre-techniques-components.module';
