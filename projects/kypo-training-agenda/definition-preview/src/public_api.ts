/*
 * Public API Surface of entry point kypo-training-agenda/definition-preview
 */
export * from './components/training-preview-components.module';
export * from './components/training-preview.component';
