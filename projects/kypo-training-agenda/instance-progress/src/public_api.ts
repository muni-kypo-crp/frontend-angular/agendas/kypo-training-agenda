/*
 * Public API Surface of entry point kypo-training-agenda/instance-progress
 */
export * from './components/training-instance-progress-components.module';
export * from './components/training-instance-progress.component';
