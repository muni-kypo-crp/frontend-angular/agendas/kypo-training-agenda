/*
 * Public API Surface of entry point kypo-training-agenda/instance-runs
 */

export * from './components/training-instance-runs-components.module';
export * from './components/training-instance-runs.component';
export * from './services/runs/training-run.service';
