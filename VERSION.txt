18.1.0 Move stepper bar from training definition to instance.
18.0.3 Fix definition upload button handling of invalid files.
18.0.2 Fix Sentinel lists elements not being displayed.
18.0.1 Fix aggregated results navigation id.
18.0.0 Update to Angular 18.
16.2.3 Update topology graph version.
16.2.2 Fix training definition dialog not displaying all definitions.
16.2.1 Remove sorting option on expire time in training instance overview.
16.2.0 Update additional components versions to latest version.
16.1.9 Update visualization versions.
16.1.8 Update topology graph version.
16.1.7 Update sentinel and component versions.
16.1.6 Fix validation of adaptive questions.
16.1.5 Fix designer table not being initialized.
16.1.4 Add better explanation to force delete dialog of an instance.
16.1.3 Update cheating detection components to work without  a deprecated Sentinel directive.
16.1.2 Update pagination to save size by component id.
16.1.1 Fix training and info phase background color.
16.1.0 Update sentinel and internal component versions.
16.0.4 Disable end time field for edit of an expired instance.
16.0.3 Update all non-cheating detection components to work without  a deprecated Sentinel directive.
16.0.2 Partial adjustment of component dependence on a deprecated sentinel directive.
16.0.1 Update file-uploading library.
16.0.0 Update to Angular 16, update local issuer to Keycloak.
15.2.4 Fix pagination changes propagation in training run overview.
15.2.3 Fix aggregated command timeline not displaying commands.
15.2.2 Cheating Detection text enhancements.
15.2.1 Fix saving of free form questions in adaptive questionnaire.
15.2.0 Implement functionality for question marking as required in adaptive training definition.
15.1.10 Fix commands not displaying.
15.1.9 Update version of command visualizations.
15.1.8 Update detection descriptions.
15.1.7 Bump version of command timeline.
15.1.6 Fix initial sorting and sorting columns of detection event table.
15.1.5 Add partial sorting support to detection event table.
15.1.4 BUmp version of api, model and command visualization.
15.1.3 Add enhancements to cheating detection edit and detail pages.
15.1.2 Fix forbidden command table and timeline.
15.1.1 Update cheating detection create page, modules and tables.
15.1.0 Integrate new version of cheating detection models and api.
15.0.8 Fix end time sorting for training instance and training run resume action display.
15.0.7 Fix faulty training run button.
15.0.6 Resolve adaptive simulator Angular Material migration.
15.0.5 Fix faulty buttons in agendas.
15.0.4 Bump version of api to fix allocation id attributes.
15.0.3 Add sorting to training run table.
15.0.2 Add allocation id to training run table.
15.0.1 Prune all broken reference and trainee graphs.
15.0.0 Update to Angular 15.
14.6.2 Add created at column to training definition.
14.6.1 Fix disabling of pool button when start time is set to current.
14.6.0 Fix ATD disabled saving when adding questions.
14.5.18 Fix Markdown text rendering for definition preview and training run.
14.5.17 Add word splitting and breaking to Definition overview.
14.5.16 Fix error in questions after every refresh.
14.5.15 Rename Authors title to Designers in ATD and LTD edit page.
14.5.14 Fix ATD notes and outcomes of length 1 not saving.
14.5.13 Add sorting options to instance overview and summary tables.
14.4.12 Bump topology graph version.
14.4.11 Fixed deletion of ATD questions and question saving without correct answers. Bump model and api versions.
14.4.10 Added text wrap to Assessment level mcq and markdown support to LTD and ATD question titles. Fixed issues with assessment level overflow, notes, outcomes and test mode.
14.4.9 Changes naming of Authors to Designers in linear and adaptive training definitions.
14.4.8 Update topology legend and command timeline display
14.4.7 Restructured cheating detection table actions. Updated detection detail date format. Added specified time proximity to time proximity stage detail.
14.4.6 Added cheating detection export.
14.4.5 Enabled cheating detection routing.
14.4.4 Reworked file structure for instance-cheating-detection-detail folder and added module provider to cheating detection event detail.
14.4.3 Remove unused file and added module provider to cheating detection event detail.
14.4.2 Enable cheating detection. Update and enhance cheating detection model with new functionality and fixes.
14.4.1 Disable cheating detection for the 22.12 release.
14.4.0 Replace sandbox id with sandbox uuid in a training run. Bump visualizations.
14.3.0 Integrate experimental version of cheating detection.
14.2.8 Fix unlock of sandboxes when deleting training run.
14.2.7 Bump hurdling visualization to integrate refactored version of progress visualization.
14.2.6 Add an automatic and systematic check of whether logging of events and commands works.
14.2.5 Fix infinite loading of pool size in instance overview table for records with deleted pools.
14.2.4 Add walkthrough visualization to visualizations for post training analysis.
14.2.3 Add command timeline for ongoing trainings in training progress for Instructor. Add access token trimming.
14.2.2 Add an option to export score of all trainees from a training instance. Change save strategy for a training instance with non-local environment.
14.2.1 Bump adaptive model simulator version.
14.2.0 Update of refactored assessment visualization. Removed elastic search dependency.
14.1.1 Fix training run results routing.
14.1.0 Optimize lazy loaded modules for instance and definition detail. Integrate adaptive model simulating tool.
14.0.1 Fix markdown errors after Sentinel update.
14.0.0 Update to Angular 14.
13.3.9 Fix instance summary routing for aggregated dashboard.
13.3.8 Fix table load event for tables without pagination.
13.3.7 Fix adaptive instance save when editing pool or sandbox definition selection. Fix adaptive access phase text overflow.
13.3.6 Fix margin in adaptive definition create. Bump statistical visualization. Fix filtering in adaptive instance overview table.
13.3.5 Fix mire techniques expansion panel description position.
13.3.4 Allow empty start time in training instance.
13.3.3 Added option to specify in the training level can be completed without commands. Bumped statistical visualization to resolve some issues.
13.3.2 Fix correct loading of consoles for topology in training run. Changed polling strategy for progress visualization.
13.3.1 Fix total number of actions in TD overview table. Bump version of topology graph and fix next phase for questionnaire.
13.3.0 Add new across instance visualizations. Fix transition to next level/phase on second request fail. Add Map of games.
13.2.3 Fix training definition selector.
13.2.2 Fix training run table pagination.
13.2.1 Show additional actions in tables.
13.2.0 Add support for movement between already accessed levels.
13.1.4 Add routing for training run visualizations tabs
13.1.3 Removed upper bound from estimated time and minimal possible solve time. Add info about variant answers above solution content. Trim answer and passkey.
13.1.2 Bump dashboard version to latest for the upcoming event.
13.1.1 Bump hurdling and overview visualizations to address issues found in the event.
13.1.0 End time of training instance is now editable for ongoing instance. Preview of a reference graph is available in training definitions with reference solution. Fix topology placeholder for preview of training definition. Add variable bearer token in the local content of the access level/phase.
13.0.2 Optimize definition preview imports.
13.0.1 Add routing for visualization tabs.
13.0.0 Update to Angular 13, CI/CD optimisation, adaptive definition improvements and bug fixes.
12.1.6 Fix loading of spinner
12.1.5 Fix default pagination for pool selection in training instances.
12.1.4 Fix adaptive progress route
12.1.3 Fix duration of training runs in instance and run detail, fix adaptive definition detail, bump visualizations.
12.1.2 Add topology legend, update environment base paths.
12.1.1 Export missing modules from library.
12.1.0 Add adaptive transition visualization, visualizations for command analysis, dashboard for organizer and Preloading of consoles for VMs.
12.0.14 New training run design. Move checkbox for variant sandboxes to training levels. Add overview of correct answers for each sandbox.
12.0.13 New version of the topology graph package - added missing configuration for Apache Guacamole.
12.0.12 Add checkbox to support creation of default content in the training definition.
12.0.11 Add detail pages for training definition and instace.
12.0.10 Fix linear questions, latest overview visualization.
12.0.9 Fix adaptive training instance edit redirect
12.0.8 Bump visualizations to latest versions.
12.0.7 Modify save strategy for definitions and instances. Change training run hint, solution and submit buttons position and design.
12.0.6 Stepper for questions, sanitize function for markdown, change input to markdown for question text, rename flag to answer and game level to training level, save user preffered pagination, rename get SSH button, fix - number of free sandboxes, movement between levels in preview mode, integration of the APG functionality, display all related questions in training phase.
12.0.5 Spinner for finished training runs, disable points for non test questions
12.0.4 Update gitlab CI
